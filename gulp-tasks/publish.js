const sequence = require('run-sequence');

module.exports = () => {
  sequence(
    'clean',
    ['html','webpack','vendor:base','vendor:publish'],
    'js:rename',
    'js:clean'
  );
};
