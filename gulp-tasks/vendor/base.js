const gulp = require('gulp'),
  paths = require('../../task-sets/paths.js');

module.exports = () => {
  return gulp.src([
    paths.package + 'reset-css/reset.css'
  ])
    .pipe(gulp.dest(paths.dist + 'css/vendor/'));
};
