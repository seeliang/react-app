const sequence = require('run-sequence');

module.exports = () => {
  sequence(
    'clean',
    'vendor:base',
  );
};
